var qrCodeScannerApp = angular.module('qrcode-app', ['ui.bootstrap','ngTouch']); // Dashboard App

qrCodeScannerApp.config(function($interpolateProvider) {
   $interpolateProvider.startSymbol('{[{');
   $interpolateProvider.endSymbol('}]}');
 });

 qrCodeScannerApp .config(['$httpProvider', function($httpProvider) {

        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    }]);
 qrCodeScannerApp.controller('qrController', function ($scope, $log, $http,$uibModal,$interval,$timeout){
  		$scope.checkListParams = {}
  		$scope.submenu = false
  		$scope.enableFooter= false
  	$scope.init = function(companyName,machineId){
		$scope.companyName = companyName
  		// $http.get('http://autoclita.com:8004/demo/api/home/GetCheckList', {
  		 $http.get('http://127.0.0.1:8000/andon/api/home/GetCheckList', {
			params: {
		 	'machineId':machineId,
		 	"companyName":companyName
			}
			}).success(function (jsonData) {
				$scope.updateEvents = {}
				$scope.updateEvents["status"] = "esclated"
  				$scope.updateEvents["questionsStatus"] = []
  				$scope.updateEvents["checklist_serial_number"] = jsonData["checkListSerialNumber"]
  				$scope.updateEvents["machine_id"] = machineId
			if(!jsonData.questionInfo.length){
				$scope.submenu = "operator"
				$scope.submitEsclation()
				// $scope.init(companyName,checkListSerialNumber)
				return
			}
  			$scope.enableFooter= true
  			

			$scope.demoFlag = jsonData.demoFlag
			$scope.heading = jsonData.heading
			$scope.checkListParams["date"] = jsonData.date
			$scope.checkListParams["shift"] = jsonData.shift
			$scope.checkListParams["checkListSerialNumber"] = jsonData.checkListSerialNumber
			$scope.checkListParams["machineInfo"] =jsonData.machineInfo[0]
			$scope.checkListParams["questionInfo"] = jsonData.questionInfo
			$scope.questionInfo = $scope.checkListParams["questionInfo"][0]
			$scope.questionInfo["displayOnly"].push({"frequency":$scope.checkListParams["questionInfo"][0].frequency})	
			console.log(JSON.stringify(jsonData.questionInfo))
			console.log("q==\n\n\n" +JSON.stringify($scope.checkListParams['questionInfo']))
		}).error(function (data, status, headers, config) {
			alert("error");
		});
  	}

  	$scope.Submit = function(status){
		
		$scope.questionStatus = {}
		$scope.questionStatus["status"] = status
		$scope.questionStatus["question_number"] = $scope.questionInfo["question_number"]
		$scope.updateEvents["questionsStatus"].push($scope.questionStatus)
		alert(JSON.stringify($scope.updateEvents))
		$scope.NextQuestion()
		}
	$scope.submitEsclation = function(){

		// $http.get('http://autoclita.com:8004/demo/api/home/SetCheckList', {
		 $http.get('http://127.0.0.1:8000/andon/api/home/SetCheckList', {
			params: {
		 	'updateEvent':$scope.updateEvents,
			'companyName':$scope.companyName
			}
			}).success(function (jsonData) {
				$scope.questionInfo={}
				if(!($scope.checkListParams["questionInfo"].length -1)){
					$scope.enableFooter = false
					$scope.submenu = "operator"
					// $scope.init()
					return
				}
			// $timeout(function() { 
			// 	$scope.NextQuestion()
			// 	}, 500);
		
		}).error(function (data, status, headers, config) {
			alert("error");
		});
}
			$scope.NextQuestion = function(){
				for(i=0;i<$scope.checkListParams["questionInfo"].length;i++){
					if($scope.checkListParams["questionInfo"][i]["question_number"] == $scope.questionStatus["question_number"] ){
						$scope.checkListParams["questionInfo"].splice(i, 1);
						$scope.frequency = $scope.checkListParams["questionInfo"][0].frequency	
						$scope.questionInfo = $scope.checkListParams["questionInfo"][0]
						$scope.questionInfo["displayOnly"].push({"frequency":$scope.checkListParams["questionInfo"][0].frequency})	
						}
					}
				if(!$scope.checkListParams["questionInfo"].length){
					$scope.submitEsclation()
					$scope.questionInfo = {}
					$scope.enableFooter = false
					$scope.submenu = "operator"
					return
				}
			}
	
});
