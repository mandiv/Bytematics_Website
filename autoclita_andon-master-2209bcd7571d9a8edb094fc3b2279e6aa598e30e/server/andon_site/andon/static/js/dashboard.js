
var dashboardApp = angular.module('dashboard-app', ['ui.bootstrap','ngTouch','dndLists', 'webcam','bcQrReader','graphPlotter']); // Dashboard App

 dashboardApp.config(function($interpolateProvider) {
   $interpolateProvider.startSymbol('{[{');
   $interpolateProvider.endSymbol('}]}');
 });

 dashboardApp.config(['$httpProvider', function($httpProvider) {

        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    }]);


dashboardApp.controller('dashboardController', function ($scope, $log, $http,$uibModal,$interval,$timeout){

	$scope.submenu = false

	$scope.SubmenuChooser = function(submenu){
		$scope.submenu = submenu
		if (!$scope.submenu){
			$scope.init()
		}
	}
	$scope.alertMessage = function(msg){
   
    	$scope.msg = msg
     	var modalInstance = $uibModal.open({
        	animation     :   true,
        	keyboard      :   true,
        	bindToController  :   true,
        	scope       :   $scope,
        	templateUrl   :   'alertMTemplate',
        	controller    :   'alertMController',
        	size        :   'md',
       });
	}

 	$scope.init = function(){
 		$scope.btnEnabled = false

		$scope.pendingCheckList = []
		$http.get('api/home/GetEsclation/', {
			params: {
			}
		}).success(function (jsonData) {
			
		
			$scope.rule = jsonData.rule
			$scope.designation = jsonData.designation
			$scope.companyName = jsonData.companyName
			
			$scope.esclatedList = []
			$scope.esclatedList = jsonData.esclatedList 
			
			
			
		}).error(function (data, status, headers, config) {
			// alert("Internet is too slow or unavailable, Refresh please");
		});
		}


	 $scope.init()
	$scope.threadFunction = function(){
			$interval($scope.init,5*60*10*100);
			
		}
	
	$scope.getCheckList = function(assetNumber){
		$scope.checkListParams={}
		 $http.get('api/home/GetCheckList/', {
			params: {
		 	'assetNumber':assetNumber,
		 	"companyName":$scope.companyName
			}
			}).success(function (jsonData) {
				
				$scope.updateEvents = {}
				$scope.updateEvents["line"] = jsonData["line"]
				$scope.updateEvents["status"] = "esclated"
  				$scope.updateEvents["questionsStatus"] = []
  				$scope.updateEvents["checklist_serial_number"] = jsonData["checkListSerialNumber"]
  				$scope.updateEvents["machine_id"] = assetNumber
  				
			if(!jsonData.questionInfo.length){
				$scope.submenu = "operator"
				$scope.submitEsclation()
				// $scope.init(companyName,checkListSerialNumber)
				return
			}
  			$scope.enableFooter= true
			$scope.submenu = "operator"


			$scope.demoFlag = jsonData.demoFlag
			$scope.heading = jsonData.heading
			// $scope.checkListParams["date"] = jsonData.date
			$scope.checkListParams["checkListSerialNumber"] = jsonData.checkListSerialNumber
			$scope.checkListParams["machineInfo"] =jsonData.machineInfo[0]
			$scope.checkListParams["questionInfo"] = jsonData.questionInfo
			$scope.questionInfo = $scope.checkListParams["questionInfo"][0]
			$scope.questionInfo["displayOnly"].push({"frequency":$scope.checkListParams["questionInfo"][0].frequency})	
			console.log(JSON.stringify(jsonData.questionInfo))
			console.log("q==\n\n\n" +JSON.stringify($scope.checkListParams['questionInfo']))
			console.log("q==\n\n\n" +JSON.stringify($scope.checkListParams['questionInfo']))
		}).error(function (data, status, headers, config) {
			$scope.alertMessage("Machine does not exist in any line !");

		});
	
	}
	$scope.GetEscaltionBeakup = function(esclationCheckListInfo){
		console.log("sdad" + JSON.stringify(esclationCheckListInfo))
		$scope.mActionBt = false
		$scope.btnEnabled = false
		$scope.machineDetails = esclationCheckListInfo
		if(esclationCheckListInfo.mType == "success"){
				$scope.getCheckList($scope.machineDetails.machine_id)
				return
			}
	 	if($scope.designation == "admin" || $scope.rule == false ){
	 	
		 	if (esclationCheckListInfo.mType == "notice"){
				$scope.verifyBtn = true
				$scope.mActionBtn = false
				$scope.GetEsclationBreakeUp()
				return
					}
				}
	 if( $scope.designation == "admin" || (esclationCheckListInfo.mType == "danger" && $scope.rule == true)){
				$scope.mActionBtn = true
				$scope.verifyBtn = false
				$scope.GetEsclationBreakeUp()
				return

			}
		}

	
	$scope.GetEsclationBreakeUp = function(){
		
		$http.get('api/home/GetEsclationBreakUp/', {
			params: {
		 	'machineId':$scope.machineDetails.machine_id
			}
			}).success(function (jsonData) {
				

				$scope.esclationDetails = jsonData.escDetails[0]
				
			var modalInstance = $uibModal.open({
        	animation     :   true,
        	keyboard      :   true,
        	bindToController  :   true,
        	scope       :   $scope,
        	templateUrl   :   'EscalationDetails/',
        	controller    :   'breakUpController',
        	size        :   	'md',
       
      		});
		}).error(function (data, status, headers, config) {
			alert("Error");
		});

	}
	$scope.Submit = function(status){
		$scope.questionStatus = {}
		$scope.questionStatus["status"] = status
		$scope.questionStatus["question_number"] = $scope.questionInfo["question_number"]
		for(i=0;i<$scope.questionInfo["displayOnly"].length;i++){
			if ($scope.questionInfo["displayOnly"][i]["Activity"] != undefined){
		
				$scope.questionStatus["question"] = $scope.questionInfo["displayOnly"][i]["Activity"]
			}
		}
		$scope.updateEvents["questionsStatus"].push($scope.questionStatus)
	
		$scope.NextQuestion()
		}
	// $scope.SendWatsappMessage = function(){
	// 	$http.get('', {

	// 	})

	// }

	$scope.submitEsclation = function(){
	

		// $http.get('http://autoclita.com:8004/demo/api/home/SetCheckList', {
		 $http.get('api/home/SetCheckList/', {
			params: {
		 	'updateEvent':$scope.updateEvents,
			'companyName':$scope.companyName
			}
			}).success(function (jsonData) {
				// $scope.SendWatsappMessage()
				$scope.questionInfo={}

				if(!($scope.checkListParams["questionInfo"].length )){

					$scope.enableFooter = false
					$scope.init()
					$scope.submenu = false
					return
				}
			// $timeout(function() { 
			// 	$scope.NextQuestion()
			// 	}, 500);
		
		}).error(function (data, status, headers, config) {
			alert("Error");
		});
}
			$scope.NextQuestion = function(){
				for(i=0;i<$scope.checkListParams["questionInfo"].length;i++){
					if($scope.checkListParams["questionInfo"][i]["question_number"] == $scope.questionStatus["question_number"] ){
						$scope.checkListParams["questionInfo"].splice(i, 1);
						$scope.questionInfo = $scope.checkListParams["questionInfo"][0]
						}
					}
				if(!$scope.checkListParams["questionInfo"].length){
					$scope.submitEsclation()
					$scope.questionInfo = {}
					$scope.init()
					$scope.enableFooter = false
					$scope.submenu = false
					
					return
				}
			}
});
dashboardApp.controller('breakUpController', function ($scope, $log, $http ,$uibModalInstance){
	$scope.solved = function(status){
		$scope.actionDict = {}
		$scope.actionDict["machine_id"] = $scope.machineDetails.machine_id
		$scope.actionDict["machine_name"] = $scope.machineDetails.machine
		$scope.actionDict["status"] =status
		$http.get('api/home/SetEventAction/', {
          	params: {
                      "actionDict" : $scope.actionDict,
                     
                  }
                  }).success(function (jsonData) {
                  	$scope.init()
                 	$uibModalInstance.dismiss('cancel');
                  }).error(function (data, status, headers, config) {
                      alert("Error")
                      $scope.alerts.push({type: 'danger', msg: data});
                 });

	}
 	$scope.ok = function () {
 		$scope.setSolvedQuestionsList = []
 	  for (i=0;i<$scope.escDetails.questions.length;i++)
 	 	{

 	 	if($scope.escDetails.questions[i]["isChecked"]){
 	 		
 	 		$scope.solvedQuestion = {}
 	 		$scope.solvedQuestion.question_number = $scope.escDetails.questions[i]["question_number"]
 	 		$scope.solvedQuestion.checklist_serial_number = $scope.escDetails.checklist_serial_number
		 	$scope.setSolvedQuestionsList.push($scope.solvedQuestion)
		 	 	}
		 }
		 if($scope.setSolvedQuestionsList.length){
		 	$http.get('api/home/SetSolvedQuestionsList/', {
          	params: {
                      "setSolvedQuestionList" : [$scope.setSolvedQuestionsList],
                  }
                  }).success(function (jsonData) {
                  	$scope.init()
                 	$uibModalInstance.dismiss('cancel');
                  }).error(function (data, status, headers, config) {
                      alert("Error")
                      $scope.alerts.push({type: 'danger', msg: data});
                 });	
		 }

 		
 	 
     }
});






// schedulingController


dashboardApp.controller('schedulingController', function ($scope, $log, $http){


	$scope.GetUserMapInformation = function(){
		$http.get('api/home/GetScheduledInformation/', {
			params: {
			}
			}).success(function (jsonData) {
				
				$scope.scheduledInfo = []
				for (i=0;i<jsonData.scheduledInfo.length;i++){
						var tmpDict = {}
						tmpDict["line"] = jsonData.scheduledInfo[i].line
						delete jsonData.scheduledInfo[i].line
						tmpDict["owners"] = jsonData.scheduledInfo[i]
						$scope.scheduledInfo.push(tmpDict)
				}
				console.log(JSON.stringify($scope.scheduledInfo[1]))

			 }).error(function (data, status, headers, config) {
                console.log("error")
                   });

			}

           
           //line hide and how
           $scope.IsVisible = false;
            $scope.ShowHide = function (index) {
            	
            	if( $scope.IsVisible == true && $scope.index1 == index ){
            		$scope.index1 = -1
            	}
            	else{
               
            		$scope.index1 = index
            		$scope.IsVisible = true;
            	}
            	              }
            //end of line hide nd how

	$scope.GetUserMapInformation()
	$scope.SetSchedule = function(line,userInfo){
		$scope.sheduleDict = {}
		$scope.scheduleDict = angular.copy(userInfo)
		$scope.scheduleDict["line"] = line
		
		if (userInfo.isScheduled){ 
				$scope.scheduleDict["status"] = "scheduled"

		}
		else{
		$scope.scheduleDict["status"] = "unSchedule"
		}
		delete $scope.scheduleDict["isScheduled"]
		$http.get('api/home/SetSchedule/', {
			params: {
				"sParams":$scope.scheduleDict
			}
			}).success(function (jsonData) {
				

			 }).error(function (data, status, headers, config) {
                alert("Error")
                $scope.alerts.push({type: 'danger', msg: data});
           });
		
		}

  });






	
  dashboardApp.controller('settingsController', function($scope,$http) {
  
  	$scope.GetFactory = function(){
  		$http.get('api/home/GetAssetNumbers/', {
			params: {
				
		 	
			}
			}).success(function (jsonData) {
    		$scope.factoryInfo = {}
    		$scope.selectedLineInfo = {}
    		$scope.GetLines()

			 }).error(function (data, status, headers, config) {
                alert("Error")
                $scope.alerts.push({type: 'danger', msg: data});
           });

  	}


  	 $scope.ReserveMachines = function(){
  		$scope.model1 = []
  			$scope.model = []
  		$http.get('api/home/GetAssetNumbers/', {
			params: {
				
		 	
			}
			}).success(function (jsonData) {
    		$scope.reserveMachines = [{"line":"Reserve Machines","assetNumbers":jsonData.reserveAssetNumbers}]
    		$scope.lineMachines = []
			$scope.lineMachines = jsonData.assetNumbersList
		  
			
				$scope.model1.push($scope.generateList($scope.reserveMachines[0]["line"],$scope.reserveMachines[0]['assetNumbers']))
				for(i=0;i<$scope.lineMachines.length;i++){
					var tmpList = []

					for (j=0;j<$scope.lineMachines[i]['assetNumbers'].length;j++){
						tmpList.push($scope.lineMachines[i]['assetNumbers'][j]["assetNumber"])
						}
				$scope.model.push($scope.generateList($scope.lineMachines[i]['line'],tmpList))
				}


			 }).error(function (data, status, headers, config) {
                alert("Error")
                $scope.alerts.push({type: 'danger', msg: data});
           });

  	}
  	$scope.ReserveMachines()
  	$scope.replacer = function(value) {
  // Filtering out properties
  	if (typeof value === 'string') {
    	return undefined;
  	}
  	return value;
	}
 
  
 $scope.onDrop = function(srcList, srcIndex, targetList,targetIndex,line) {

 
 	$scope.targetDict = {}
 	sourceDict = srcList[srcIndex].labelFunc(srcIndex)
 
      // Copy the item from source to target.
      targetList.splice(targetIndex, 0, srcList[srcIndex]);

      // Remove the item from the source, possibly correcting the index first.
      // We must do this immediately, otherwise ng-repeat complains about duplicates.
      if (srcList == targetList && targetIndex <= srcIndex) srcIndex++;
     	srcList.splice(srcIndex, 1);
     	

      // By returning true from dnd-drop we signalize we already inserted the item.
      if (line == "Reserve Machines"){
      		
      		$scope.targetDict["line"] = line
      		$scope.targetDict["assetNumber"] =sourceDict.assetNumber
      		$scope.targetDict["order"] = targetIndex
   
      		}
      else{

      	for(i=0;i< $scope.lineMachines.length;i++){
      	
      		if ($scope.lineMachines[i]["line"] == line){
      			$scope.targetDict["line"] = line
      			$scope.targetDict["assetNumber"] =sourceDict.assetNumber
      			$scope.targetDict["order"] = targetIndex
      			
      		}
      	}
      }

     
      $http.get('api/home/SetMachineMapping/', {
			params: {
				"tParams" : $scope.targetDict,
				"sParams": sourceDict
		 	
			}
			}).success(function (jsonData) {
				 }).error(function (data, status, headers, config) {
                alert("Error")
                $scope.alerts.push({type: 'danger', msg: data});
           });
     
      return true;
    };

   $scope.generateList =  function (id,li) {
     
      return li.map(function(letter) {

        // angular-drag-and-drop-lists usually serializes the objects to drag, thus we
        // can not transfer functions on the objects. However, as this fiddle uses dnd-callback
        // to move the objects directly without serialization, we can use a function reference
        // on the item here.
        return {
          labelFunc: function(index) {
          	var tmp = {}
          	tmp = {"assetNumber":letter,"line":id}

            return tmp;
          }
        };
      });
    }
    
    //line hide and show
           $scope.IsVisible = false;
            $scope.ShowHide = function (index) {
            	
            	if( $scope.IsVisible == true && $scope.index1 == index ){
            		$scope.index1 = -1
            	}
            	else{
               
            		$scope.index1 = index
            		$scope.IsVisible = true;
            	}
            	              }
            //end of line hide nd show

  

  });
dashboardApp.controller("qrCodeScannerController", function ($scope, $log, $http,$uibModal,$interval,$timeout){


  $scope.start = function() {
        
      $scope.cameraRequested = true;
  }
   $scope.start()

  

  
  $scope.processURLfromQR = function (assetNumber ) {

       $scope.getCheckList(assetNumber)
		 
  }


});


dashboardApp.controller("alertMController", function ($window,$scope, $log, $http,$uibModal,$interval,$timeout,$uibModalInstance){
$scope.Ok = function(){
$uibModalInstance.dismiss('cancel');
$window.location.reload();
}

});






dashboardApp.controller('reportController',function ($scope, $log, $http,$uibModal,$interval,$timeout){
   
 $scope.GetPlantParams = function(){
 	$scope.reportParams = {}
		$http.get('api/home/GetPlantParams/', {
			params: {
				
		 	
			}
			}).success(function (jsonData) {
				$scope.plantPrams = jsonData
				$scope.reportParams["line"] = jsonData[0]["line"]
				 $scope.lineChange()
			
				// $scope.reportParams["machine"] = jsonData[0]["machine"]

			 }).error(function (data, status, headers, config) {
                console.log("Error")
                $scope.alerts.push({type: 'danger', msg: data});
           });

 }
 $scope.GetPlantParams()

 $scope.lineChange = function(){
 	 $scope.machineInfo  = []
 	for (i=0;i<$scope.plantPrams.length;i++){

        if($scope.plantPrams[i]["line"] == $scope.reportParams['line']){
            // if ($scope.plantPrams[i]["line"] == "All"){
                $scope.machineInfo = $scope.plantPrams[i].machine
              // }
            }
          }
        $scope.reportParams['machine'] = $scope.machineInfo[0]
        $scope.GetPlantHealth()
        

 }

 $scope.GetPlantHealth = function(){

		$http.get('api/home/GetPlantHealth/', {
			params: {
				"reportParams":$scope.reportParams
			}
			}).success(function (jsonData) {
				$scope.plantHealthIndicator = jsonData
			
			 }).error(function (data, status, headers, config) {
                console.log("Error")
                $scope.alerts.push({type: 'danger', msg: data});
           });

 }
$scope.machineChange = function(){
	$scope.GetPlantHealth()

}

});
