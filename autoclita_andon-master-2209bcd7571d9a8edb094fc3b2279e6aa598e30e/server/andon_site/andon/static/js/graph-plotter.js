var graphPlotter = angular.module('graphPlotter', []);

graphPlotter.directive('linePlot', [function () {
    function linkFunc($scope, element, attrs) {
        $scope.$watch('graphPlots' ,function (plots) {
        	
        	var layout={title: 'BYTEMATICS',xaxis:{ title: 'x Axis',titlefont: {family: 'Courier New, monospace',size: 18,color: '#7f7f7f'}},
                                         yaxis:{title: 'y Axis',titlefont: {family: 'Courier New, monospace',size: 18,color: '#ed1262'}},
                                         height: 500, width: 550,paper_bgcolor: '#c7c7c7',plot_bgcolor: '#ed1262'
                    };
            Plotly.newPlot(element[0], plots,layout);
        });

    }

    return {
    link: linkFunc
    };
}]);
