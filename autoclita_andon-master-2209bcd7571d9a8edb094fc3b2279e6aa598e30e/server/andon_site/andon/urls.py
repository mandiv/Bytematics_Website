from django.contrib import admin
from django.conf.urls import include, url


from . import views

urlpatterns = [
#Authentication and other user account related views
url(r'^login/', 	views.Login ,	 name = "Login"),
url(r'^logout/', 	views.Logout ,	 name = "Logout"),
#register


url(r'^password_change/$',       views.passwordChange,           name = 'passwordChange'),
url(r'^password_reset/$',       views.password_reset,           name = 'password_reset'),
url(r'^password_reset/done/$',  views.password_reset_done,      name = 'password_reset_done'),
url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.password_reset_confirm, name = 'password_reset_confirm'),
url(r'^reset/done/$',           views.password_reset_complete,  name = 'password_reset_complete'),


url(r'^$',		views.home,	name = 'home'),
url(r'^home/$',		views.home,	name = 'home'),
url(r'^dashboard/$',    views.home,                     name = 'home'),
url(r'^operatorTemplate/$',    views.operatorTemplate,                     name = 'operatorTemplate'),
url(r'EscalationDetails/$',    views.EscalationDetails,                     name = 'EscalationDetails'),

url(r'^alertMTemplate/$',        views.alertMTemplate,   name = 'alertMTemplate'),
url(r'^settings/$',        views.settings,   name = 'settings'),
url(r'machineTemplate/$',        views.machineTemplate,   name = 'machineTemplate'),

url(r'^scheduling/$',        views.scheduling,   name = 'scheduling'),

url(r'^qrscanner/$',        views.qrscanner,   name = 'qrscanner'),
url(r'^report/$',        views.report,   name = 'report'),
# url(r'^qrcode/$',        views.serve_qr_code_image,   name = 'serve_qr_code_image'),


#api view

url(r'api/home/UploadHierachyInfo/$',               views.UploadHierachyInfo,                   name = 'UploadHierachyInfo'),
url(r'api/home/UploadCheckList/$',               views.UploadCheckList,                   name = 'UploadCheckList'),
url(r'api/home/UploadMachineInfo/$',               views.UploadMachineInfo,                   name = 'UploadMachineInfo'),
url(r'api/home/GetActionCheckLists/$',               views.GetActionCheckLists,                   name = 'GetActionCheckLists'),
url(r'api/home/GetEscalationList/$',               views.GetEscalationList,                   name = 'GetEscalationList'),
url(r'api/home/GetCheckList/$',               views.GetCheckList,                   name = 'GetCheckList'),
url(r'api/home/SetCheckList/$',               views.SetCheckList,                   name = 'SetCheckList'),
url(r'api/home/SetEventAction/$',               views.SetEventAction,                   name = 'SetEventAction'),
url(r'api/home/GetEsclationBreakUp/$',               views.GetEsclationBreakUp,                   name = 'GetEsclationBreakUp'),
url(r'api/home/RemoveUser/$',               views.RemoveUser,                   name = 'RemoveUser'),
url(r'api/home/SetdemoFlag/$',               views.SetdemoFlag,                   name = 'SetdemoFlag'),
url(r'api/home/GetCheckListFomQrCode/$',               views.GetCheckListFomQrCode,                   name = 'GetCheckListFomQrCode'),

url(r'api/home/GetCheckListTemplates/$',               views.GetCheckListTemplates,                   name = 'GetCheckListTemplates'),
url(r'api/home/SetMachines/$',               views.SetMachines,                   name = 'SetMachines'),
url(r'api/home/GetEsclation/$',               views.GetEsclation,                   name = 'GetEsclation'),
url(r'api/home/GetMachines/$',               views.GetMachines,                   name = 'GetMachines'),
url(r'api/home/GetMachineDetails/$',               views.GetMachineDetails,                   name = 'GetMachineDetails'),
url(r'api/home/RemoveMahine/$',               views.RemoveMahine,                   name = 'RemoveMahine'),
url(r'api/home/GetScheduledInformation/$',               views.GetScheduledInformation,                   name = 'GetScheduledInformation'),
url(r'api/home/SetSchedule/$',               views.SetSchedule,                   name = 'SetSchedule'),
url(r'api/home/GetAssetNumbers/$',               views.GetAssetNumbers,                   name = 'GetAssetNumbers'),
url(r'api/home/SetMachineMapping/$',               views.SetMachineMapping,                   name = 'SetMAchineMapping'),

#url(r'api/home/GetPlantParams/$',               views.GetPlantParams,                   name = 'GetPlantParams'),
#url(r'api/home/GetPlantHealth/$',               views.GetPlantHealth,                   name = 'GetPlantHealth'),

]
