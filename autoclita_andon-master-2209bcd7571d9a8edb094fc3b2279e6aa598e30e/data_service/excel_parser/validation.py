import math
from clita_legacy.dataconnector import mongoConnector
class Validation:
	def __init__(self,companyName):
		self.companyName= companyName
		self.checkListSerialNumberList = []
	def GetCheckList(self):
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.companyName)
                if dbc is None:
                        print err.errDict["ERR_DB_NONE"] % self.companyName
                        return
                collection = dbh.getCollection("checklistsCollection")
                if collection is None:
                        print err.errDict["ERR_COLLECTION_NONE"] % 'checklistsCollection'
                        return
		docs = collection.find({},{"_id":False})
		docList = [i for i in docs]
		if docList:
			for i in docList:
				self.checkListSerialNumberList.append(i["machineInfo"]["checklist_serial_number"])
		return
				
	def Validation(self,value,rule):
                errDict = {}
                errDict["status"] = "Success"
                if rule == "headerCheck":
                        for i in list(value.columns):
                                if isinstance(i,unicode):
                                        return errDict
                        errDict["status"] = "ERROR"
                        return errDict
                elif rule == "headerValueCheck":
                        if not isinstance(value, unicode):
                                errDict["status"] = "ERROR"
                                return errDict
                        return errDict

                elif rule == unicode:
                        if not math.isnan(float(value[3])):
                                value[3] = int(value[3])
                        if not isinstance(value[0], unicode):
                                errDict["status"] = "ERROR"
                                errDict["ERROR:"] = "Name is not Entered......."
                                return  errDict
                        elif not isinstance(value[1], unicode):
                                errDict["status"] = "ERROR"
                                errDict["ERROR:"] = value[0]+" email is not Entered........"
                                return  errDict
                        elif not isinstance(value[2], int):
                                errDict["status"] = "ERROR"
                                errDict["ERROR:"] = value[0]+" Phone number is not Entered....."
                                return  errDict
                        elif not isinstance(value[3], int):
                                errDict["status"] = "ERROR"
                                errDict["ERROR:"] = value[0] +" time is not Entered........... "
                                return  errDict
                        elif not isinstance(value[4], unicode):
                                errDict["status"] = "ERROR"
				errDict["ERROR:"] = value[0] +" designation is not Entered.........."
                                return  errDict
                        if value[1].split('@')[-1].split('.')[0] != self.companyName:
                                errDict["status"] = "ERROR"
                                errDict["ERROR:"] ="use " +value[0] +" valid official email address.........."
                                return  errDict
                        if len(str(value[2])) != 10:
                                errDict["status"] = "ERROR"

                                errDict["ERROR:"] = "use"+value[0] +" valid phone number.."

                                return  errDict
                        if value[3] < 0:
                                errDict["status"] = "ERROR"
                                errDict["ERROR:"] = "use " +value[0] +" time should be a valid...."
                                return  errDict
		elif rule == "checkListCheck":
			if not isinstance(value, unicode):
                                errDict["status"] = "ERROR"
                                errDict["ERROR"] = "please enter template...."
                                return errDict
			else:

				if value.lower() == "na":
					errDict["status"] ='success'
					return errDict

				if value not in self.checkListSerialNumberList:
				 	errDict["status"] = "ERROR"
                                	errDict["ERROR"] = "Please upload "+ value +" cheCkList........"
					return errDict
		elif rule == "assetInfoCheck":
			print "========\n\n\n\n\n",value
			if not isinstance(value["assetNumber"], unicode):
                                errDict["status"] = "ERROR"
                                errDict["ERROR:"] ="Please check asset number...."
				return errDict
			for k,v in value.iteritems():
				if not isinstance(v, unicode):
                                	errDict["status"] = "ERROR"
                                	errDict["ERROR:"] = "Please Check " + k +" for " + value["assetNumber"] + "......"
					return errDict


                return  errDict

