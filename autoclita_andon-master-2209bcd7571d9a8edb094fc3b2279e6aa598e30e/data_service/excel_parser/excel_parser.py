import os
import pandas as pd
from clita_legacy.dataconnector import mongoConnector
from datetime import datetime
from collections import OrderedDict
from validation import Validation
class ExcelParser:
	def __init__(self,companyName):
		self.companyName = companyName

		self.baseDir = '../../server/andon_site/andon/static/andon_uploads/'+companyName +'/'
		self.errorDict = {}
		self.errorDict["status"] = "Success"
		
	def GetHeaderSkipCount(self,Sheet,rule):
		skipRowList = []
		if rule == 'unicode':
			for i in range(0,len(Sheet)):
				if isinstance(Sheet[i], unicode):
					skipRowList.append(i)
			return skipRowList
		elif rule == "heading":
			columnValues = Sheet[Sheet.columns[0]].values
        		# xxx need to write genreic code to find skip count
        		for i in range(len(columnValues)):
           			if columnValues[i] == 'Line Informations':
					return i

	def ParseExcelFiles(self,fileName,rule):
		rDict = {}
   		xlsfiles = None
		factoryInfoList = []
		tmpList = []
		if not os.path.exists(self.baseDir):
			rDict["status"] = "ERROR"
			rDict["ERROR"] = "There is no folder " + self.companyName + ",Please create " +  self.companyName + " folder ........"
			return rDict
		if rule == "hierarchyInfo":
			fileName =  self.baseDir + "hierarchy_info" + "/"+fileName + ".xlsx"	
			if not os.path.exists(self.baseDir + "hierarchy_info"):
				rDict["status"] = "ERROR"
				rDict["ERROR"] = "There is no hierarchy_info folder inside " + self.companyName + ",Please create  hierarchy_info folder inside " +  self.companyName + " ........"
				return rDict
			rDict = self.ParseHierarchyInfo(fileName).copy()
			print rDict
			if rDict["status"] == "ERROR":
				return rDict
		elif rule == "machineInfo":
			fileName =  self.baseDir + "machine_info" + "/"+fileName + ".xlsx"
			if not os.path.exists(self.baseDir + "machine_info"):
				rDict["status"] = "ERROR"
				rDict["ERROR"] = "There is no machine_info folder inside " + self.companyName + ",Please create  machine_info folder inside " +  self.companyName + "........"
				return rDict
			rDict= self.ParseMachineInfo(fileName)
			if rDict["status"] == "ERROR":
				return rDict
		rDict["status"] = "Success"
		return rDict
	def ParseMachineInfo(self,fileName):

		sheetName = "List of Machines"
		errorCheckDict = {}
		errorCheckDict["status"]  = "Success"
		
		if not  os.path.isfile(fileName):
			errorCheckDict["status"] = "ERROR"
			errorCheckDict["ERROR"] = "There is no file in " + self.companyName + " Folder"
			return errorCheckDict
		sheet = self.GetSheet(fileName,sheetName)
		parseMList = sheet.to_dict(orient="records")
		headers = list(sheet.columns)
		machineUpdateInfo = {}
		vl = Validation(self.companyName)
               	errcheckdict = vl.Validation(headers,"headercheck")
                if errcheckdict["status"] == "ERROR":
			errcheckdict["ERROR"] = "There is missing header......."
                        return errcheckdict

		machineUpdateInfo["headers"] = headers
		machineUpdateInfo["date"] = datetime.today()
		machineUpdateInfo["uploadInfo"] = "assetNumberInfo"
		self.SetParseInfo('andonFactoryHierarchyCollection',[machineUpdateInfo],{"date":machineUpdateInfo["date"]})
		mList = []
		assnList = []
		vl.GetCheckList()
		for i in parseMList:
			
			assnDict = {}
			assnDict["date"] = datetime.today()
			if headers[0] in i.keys():
				i["assetNumber"] = i.pop(headers[0])
				errCheckDict = vl.Validation(i,"assetInfoCheck")
                		if errCheckDict["status"] == "ERROR":
                        		return errCheckDict

				assnDict["assetNumber"] = i["assetNumber"]
			if headers[1] in i.keys():
					i["machineName"] = i.pop(headers[1])
			if headers[2] in i.keys():
					errCheckDict = vl.Validation(i[headers[2]],"checkListCheck")
                			if errCheckDict["status"] == "ERROR":
                        			return errCheckDict
					i["checkListSerialNumber"] = i.pop(headers[2])
			i["date"] = datetime.today()
			mList.append(i)
			assnList.append(assnDict)
			self.SetParseInfo('andonMachinesInfoCollection',[i],{"date":i["date"]})
			self.SetParseInfo('andonReserveMachines',[assnDict],{"date":machineUpdateInfo["date"]})
		errorCheckDict["assetNumberList"] = assnList
		return errorCheckDict
	def ParseHierarchyInfo(self,fileName):
		sheetName = "Escalation Hierarchy"
		queryDict = {}
		rDict = {}
		if not  os.path.isfile(fileName):
			rDict["status"] = "ERROR"
			rDict["error"] = "ERRROR:There is no file in " + self.companyName + " Folder........."
			return rDict
		sheet = self.GetSheet(fileName,sheetName)
		rDict = self.SetFactoryInfo(fileName,sheetName)
		if rDict["status"] == "ERROR":
			return rDict
		queryDict["date"] = datetime.today()
		self.SetParseInfo('andonLineInfoCollection',rDict["lineInfoList"],queryDict)
		return rDict
	def SetFactoryInfo(self,fileName,sheetName):
		factoryInfo = {}
		self.designationInfo = []
		sheet = self.GetSheet(fileName,sheetName)
		headDf = sheet.iloc[0:1]
                headDf = headDf.loc[:, ~headDf.columns.str.contains('^Unnamed')]
		factoryInfo["headers"] = []
		factoryInfo["headers"] = list(headDf.columns)
		queryDict = {}
		factoryInfo["date"] = datetime.today()
		factoryInfo["uploadInfo"] = "factoryHierarchy"
		firstColumn = self.GetColumnsRows(sheet,'columnWise',0,0)
   		skipRowList = self.GetHeaderSkipCount(firstColumn,"unicode")
		firstColumn = firstColumn.dropna(how="all")
		firstColumn = firstColumn.values
		count = 0
		reDict = {}
		reDict["status"] = "Success"
		reDict["lineInfoList"] = []
		reDict["userList"] = []
		sheet = sheet.iloc[:,1:]
		for i in skipRowList:
			count +=1
			tmpDict = {}
			queryDict = {}
			if count == len(skipRowList):
				subSheet_1 = sheet.iloc[i:]
				rDict = self.SetLineInfo(subSheet_1)
				if rDict["status"] == "ERROR":
					return rDict 
				tmpDict = rDict["desInfoDict"].copy()
				owners = rDict["owners"]
				tmpDict["line"] = firstColumn[count-1]
				tmpDict["date"] = datetime.today()
			else:
				subSheet_1 = self.GetColumnsRows(sheet,'rowWise',i,skipRowList[count])
				rDict= self.SetLineInfo(subSheet_1)
				if rDict["status"] == "ERROR":
					return rDict 
				tmpDict = rDict["desInfoDict"].copy()
				owners = rDict["owners"]
				tmpDict["line"] = firstColumn[count-1]
				tmpDict["date"] = datetime.today()
			reDict["userList"] +=owners
			reDict["lineInfoList"].append(tmpDict)
		self.designationInfo = [dict(t) for t in set([tuple(d.items()) for d in self.designationInfo])]
		self.designationInfo.sort(key=lambda x:x["hierarchyOrder"], reverse=True)
		factoryInfo["designationInfo"] = [i["designation"] for i in self.designationInfo]
		self.SetParseInfo('andonFactoryHierarchyCollection',[factoryInfo],{"date":factoryInfo["date"]})
		return reDict

	def SetParseInfo(self,collectionName,updateList,queryDict):
		mg = mongoConnector.MongoConnector.getInstance()
        	db= mg.getDatabaseClient(self.companyName)
		for i in updateList:
			mg.updateCollection_dash(queryDict,collectionName ,i)
		return
	def SetLineInfo(self,subSheet):
		reDict = {}
		reDict["status"] = "Succees"
		reDict["desInfoDict"] = {}
		reDict["owners"] = []
		tDict = {}
		start = 0
		owners = []
		for i in range(0,len(subSheet.columns)/5):
			end = start + 5
			df = subSheet.iloc[:,start:end]
			vl = Validation(self.companyName)
			errcheckdict = vl.Validation(df,"headercheck")
			if errcheckdict["status"] == "ERROR":
					errcheckdict["ERROR"] = "there is no header after "+ str( 1 + i+start) +" column"
					return errcheckdict
			if isinstance(df[df.columns[0]].values[0], unicode):
				header = list(df.columns)[0]
				errcheckdict = vl.Validation(df[df.columns[0]].values[0],"headerValueCheck")
				if errcheckdict["status"] == "ERROR":
					errcheckdict["ERROR"] = list(df.columns)[0] + "is not entered..."
					return errcheckdict
				reDict["desInfoDict"][list(df.columns)[0]] = df[df.columns[0]].values[0]
			df = df.iloc[1:]
			df = df.dropna(how="all")
			for index, row in df.iterrows():
				oInfo = {}
				errCheckDict = vl.Validation(row,unicode)
				if errCheckDict["status"] == "ERROR":
					return errCheckDict

				if str(row[4]) not in reDict["desInfoDict"].keys():
					reDict["desInfoDict"][row[4]] = []


				oInfo["name"] = row[0]
				oInfo["email"] = row[1]
				oInfo["phone"] = row[2]
				oInfo["Time"] = row[3]
				if i==1:
					oInfo["rule"] = True
				else:
					oInfo["rule"] = False
				reDict["desInfoDict"][row[4]].append(oInfo)
				oInfo["designation"] = row[4]
				oInfo["hierarchyOrder"] = i
				self.designationInfo.append({"designation":row[4],"hierarchyOrder":i})
				reDict["owners"].append(oInfo)
			start = end
		return reDict
	def GetSheet(self,fileName,sheetName):
		file = pd.ExcelFile(fileName)
        	sheet = file.parse(sheetName)
		return sheet
	def GetColumnsRows(self,sheet,rule,start,end):
		if rule == "columnWise":
			if not end:
				subSheet = sheet.iloc[:,0]
			else:
				subSheet = sheet.iloc[:,start:end]
		else:
			if end:
				subSheet = sheet.iloc[start:end]
		return subSheet
