import collections
import pyexcel_xlsx
import datetime
import os
#import cv2
import numpy as np
import os

class ExcelParser():
    "This will pass the smaple checklist template to be read into db"

    def __init__(self,companyName):
        #self.baseDir = '../server/clita_legacy/legacy/static/checklists/'+companyName +'/'
        self.baseDir = '../../server/clita_legacy/legacy/static/checklists/'+companyName +'/'
        #self.baseDir = '../server/clita_legacy/legacy/static/checklists/'
        self.photoLocationSizeNominal = (100, 100 ,3)
        self.photoActionSizeNominal = [50,50,3]
        pass

    def ReadExcel(self, excelFileName):
        self._path = self.baseDir + excelFileName + '/' + excelFileName + '.xlsx'
        self.dataRaw = pyexcel_xlsx.get_data(self._path)
        self._machineInfoRaw = self.dataRaw['machine_info']
        self._machineInfoDict = dict()
        for self._item in self._machineInfoRaw:
            if (self._item[0] == 'checklist_serial_number' or \
                            self._item[0] == 'plant' or \
                            self._item[0] == 'pu_shop_dept' or \
                            self._item[0] == 'cell_circle' or \
                            self._item[0] == 'machine_equipment_workstation_number' or \
                            self._item[0] == 'machine_equipment_workstation_name' or \
                            self._item[0] == 'checklist_heading' or \
                            self._item[0] == 'version'):
                self._machineInfoDict[self._item[0]] = self._item[1]
            else:
                self._machineInfoDict[self._item[0]] = self._item[1:]

        self._usersList = []
        self._k = 0
        for self._item in self._machineInfoDict['owner_email']:
            self._user = dict()
            self._user['email'] = self._item
            self._user['name'] = self._machineInfoDict['owner_name'][self._k]
            self._user['role'] = self._machineInfoDict['owner_role'][self._k]
            self._user['id'] = self._machineInfoDict['owner_id'][self._k]
            self._k  =self._k + 1
            self._usersList.append(self._user)
        self._k = 0
        for self._item in self._machineInfoDict['stakeholder_email']:
            self._user = dict()
            self._user['email'] = self._item
            self._user['name'] = self._machineInfoDict['stakeholder_name'][self._k]
            self._user['role'] = self._machineInfoDict['stakeholder_role'][self._k]
            self._user['id'] = self._machineInfoDict['stakeholder_id'][self._k]
            self._k = self._k + 1
            self._usersList.append(self._user)

        self._operatorsList = []
        self._k = 0
        for self._item in self._machineInfoDict['operator_id']:
            self._operator = dict()
            self._operator['operator_id'] = self._item
            self._operator['operator_name'] = self._machineInfoDict['operator_name'][self._k]
            self._operator['operator_role'] = self._machineInfoDict['operator_role'][self._k]
            self._k = self._k + 1
            self._operatorsList.append(self._operator)

        self._questionsRaw = self.dataRaw['questions']
        self._questionsList = []
        for self._k in range(1, len(self._questionsRaw)):
            self._questionDict = collections.OrderedDict()
            for self._m in range(6):
		if self._questionsRaw[self._k][self._m] is not None:
                	self._questionDict[self._questionsRaw[0][self._m]] = self._questionsRaw[self._k][self._m]
		else:
                	self._questionDict[self._questionsRaw[0][self._m]] = None 
                print self._questionDict[self._questionsRaw[0][self._m]]
            # self._questionDict['displayOnly'] = collections.OrderedDict()
            self._questionList = []
            for self._n in range(6, len(self._questionsRaw[0])):
                self._tempteroryDict = dict()
                self._tempteroryDict[self._questionsRaw[0][self._n]] = self._questionsRaw[self._k][self._n]
                self._questionList.append(self._tempteroryDict)
                # self._questionDict['displayOnly'][self._questionsRaw[0][self._n]] = self._questionsRaw[self._k][self._n]
            self._questionDict['displayOnly'] = self._questionList
            self._questionsList.append(self._questionDict)

        self._usersFromQuestions = []
        for self._item in self._questionsList:
            if self._item['owners'] != None:
                self._usersFromQuestions = self._usersFromQuestions + self._item['owners'].split(',')
            if self._item['stakeholders'] != None:
                self._usersFromQuestions = self._usersFromQuestions + self._item['stakeholders'].split(',')
        if self._usersFromQuestions != None:
            self._usersFromQuestions = list(set(self._usersFromQuestions))
            for self._item in self._usersFromQuestions:
                self._user = dict()
                self._user['email'] = self._item
                self._usersList.append(self._user)



        self._translation = dict()
        for item in self.dataRaw['translation']:
            self._translation[item[0]] = item[1]

        self.checkList = dict()
        self.checkList['machineInfo'] = self._machineInfoDict
        self.checkList['questions'] = self._questionsList
        self.checkList['translation'] = self._translation
	print "checkListInfo==\n",self.checkList,"\n===usertList=\n",self._usersList,"\n===operatorList===\n"
        return self.checkList, self._usersList, self._operatorsList

    def ValidateExcel(self, excelFileName):
        self._path = self.baseDir + excelFileName + '/' + excelFileName + '.xlsx'
        self.dataRaw = pyexcel_xlsx.get_data(self._path)
        self.sampleData = pyexcel_xlsx.get_data(self.baseDir + 'sample_checklist.xlsx')

        if self.sampleData.keys() != self.dataRaw.keys():
            print 'wrong sheet names for', excelFileName
            return 'notOk'

        for self._k in range(len(self.sampleData['machine_info'])):
            if self.sampleData['machine_info'][self._k][0] == self.dataRaw['machine_info'][self._k][0] == self.dataRaw['translation'][self._k][0]:
                pass
            else:
                print 'Check for keyword error in machine_info and translation, raw no: ', self._k
                return 'notOk'

            if len(self.dataRaw['machine_info'][self._k]) < 2:
                print 'Insufficient data machine_info for ', self.dataRaw['machine_info'][self._k]
                return 'notOk'

            if len(self.dataRaw['translation'][self._k]) < 2:
                print 'Insufficient data translation for ', self.dataRaw['translation'][self._k]
                return 'notOk'

        self._ownerLineIndexBegin = 1
        if len(self.dataRaw['machine_info'][self._ownerLineIndexBegin]) == len(self.dataRaw['machine_info'][self._ownerLineIndexBegin + 1 ]) == len(self.dataRaw['machine_info'][self._ownerLineIndexBegin + 2]) == len(self.dataRaw['machine_info'][self._ownerLineIndexBegin + 3]):
            pass
        else:
            print 'incorrect owner entries'
            return 'notOk'

        self._stakeholderLineIndexBegin = 5
        if len(self.dataRaw['machine_info'][self._stakeholderLineIndexBegin]) == len(
                self.dataRaw['machine_info'][self._stakeholderLineIndexBegin + 1]) == len(
                self.dataRaw['machine_info'][self._stakeholderLineIndexBegin + 2]) == len(
                self.dataRaw['machine_info'][self._stakeholderLineIndexBegin + 3]):
            pass
        else:
            print 'incorrect stakeholder entries'
            return 'notOk'

        self._operatorLineIndexBegin = 9
        if len(self.dataRaw['machine_info'][self._operatorLineIndexBegin]) == len(
                self.dataRaw['machine_info'][self._operatorLineIndexBegin + 1]) == len(
                self.dataRaw['machine_info'][self._operatorLineIndexBegin + 2]):
            pass
        else:
            print 'incorrect operator entries'
            return 'notOk'

        if self.sampleData['questions'][0][:4] != self.dataRaw['questions'][0][:4]:
            print 'incorrect operator entries'
            return 'notOk'

        self._setOfDispKeys = set(self.dataRaw['questions'][0][4:])
        for self._item in self._setOfDispKeys:
            if self._item.find('.') != -1:
                print 'Display heading ---', self._item, '---- has a . which Mongodb is not happy with'
                return 'notOk'

        self._lenQuestionsRaw1 = len(self.dataRaw['questions'][0])
        for self._item in self.dataRaw['questions']:
            if len(self._item) != self._lenQuestionsRaw1:
                print 'Empty cells in questions sheet'
                return 'notOk'

        return 'ok'

    def CreateUserCollection(self):
        pass

    def ResizePhoto(self, fileName, typeOfPhoto = 'location'):
        self._photo = cv2.imread(fileName)
        if typeOfPhoto == 'action':
            self._photoSizeNominal = self.photoActionSizeNominal
        else:
            self._photoSizeNominal = self.photoLocationSizeNominal
        self._divisionFactor = (np.array(self._photo.shape[:2]) / np.array(self._photoSizeNominal[:2]) ).max()
        if self._divisionFactor == 1:
            return None
        else:
            self._photoNominal = cv2.resize(self._photo, tuple(np.array(self._photo.shape[:2][::-1]) / self._divisionFactor))
        # cv2.imwrite(fileName, self._photoNominal)
            return self._photoNominal

    def ResizeAllPhotos(self):
        self._dirs = os.listdir(self.baseDir)
        for self._item in self._dirs:
            if os.path.isdir(self.baseDir + self._item):
                self._files = os.listdir(self.baseDir + self._item)
                for self._file in self._files:
                    if os.path.isfile(self.baseDir + self._item + '/' + self._file):
                        if len(self._file) > 4 and ( self._file[-4:].lower() == '.jpg'):
                            self._photoNominal = self.ResizePhoto(self.baseDir + self._item + '/' + self._file, typeOfPhoto='location')
                            if self._photoNominal != None:
                                cv2.imwrite(self.baseDir + self._item + '/' + self._file, self._photoNominal)
                                print self.baseDir + self._item + '/' + self._file

if __name__ == '__main__':
    excelParser = ExcelParser("legacy")
    self = excelParser
    #excelParser.ValidateExcel("Belt_Grind")	
    excelParser.ReadExcel('mc_18_puma_125')




