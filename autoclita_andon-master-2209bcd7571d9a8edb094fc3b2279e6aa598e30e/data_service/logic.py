from clita_legacy.dataconnector import mongoConnector
from clita_legacy.errcode import errcode as err
#from escalation import escalation
from checklist_info import checkListStatus
from datetime import date,time,datetime,timedelta
import collections
from threading import Timer
from twilio_connector import MessageConnector
class Logic():
	def __init__(self,dbName):
		self.dbName = dbName
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
		if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
			return 
                collection = dbh.getCollection("flagCollection")
		if collection is None:
			print err.errDict["ERR_COLLECTION_NONE"] % 'checklistsCollection'
			return
		docs = collection.find({},{"_id":False})
		self.demoFlag = False
		docList = [i for i in docs]
		if docList:
			self.demoFlag = docList[0]["demoFlag"]


	def GetCheckListTemplate(self,useremail,is_superuser):
		checkList = []
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
		if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
			return 
                collection = dbh.getCollection("checklistsCollection")
		if collection is None:
			print err.errDict["ERR_COLLECTION_NONE"] % 'checklistsCollection'
			return
		print "useremail==\n\n\n",useremail,is_superuser
		if is_superuser:
			docs = collection.aggregate([{"$match":{}},{"$group":{"_id":"$machineInfo.checklist_serial_number",'finalDate':{"$last":"$date"},"questions":{"$addToSet":"$questions"}}}])
		else: 
			docs = collection.aggregate([{"$match":{"machineInfo.owner_email":useremail}},{"$group":{"_id":"$machineInfo.checklist_serial_number",'finalDate':{"$last":"$date"},"questions":{"$addToSet":"$questions"}}}]) 
		docList = [i for i in docs]
		if docList:
			for i in docList:
				tempDict = {}
				tempDict["checkListSerialNumber"] = i["_id"]
				tempDict["qLength"] = len(i["questions"][0])
				checkList.append(tempDict)
		return checkList	
	def GetMachineDetails(self,mParams):
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
                        return
		collection = dbh.getCollection("andonMachinesCollection")
		if collection is None:
			print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andon MachinesCollection'
			return
		docs = collection.find(mParams,{"_id":False})
		docList = [i for i in docs]
		return docList
	def GetUserInfo(self,useremail):
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
                        return
		collection = dbh.getCollection("andonUsersCollection")
		if collection is None:
			print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andon MachinesCollection'
			return
		docs = collection.find({"email":useremail},{"_id":False})
		docList = [i for i in docs]
		return docList
	def GetMapLines(self,useremail):
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
                        return
		collection = dbh.getCollection("andonLineScheduleCollection")
		if collection is None:
			print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andonScheduleLineCollection'
			return
		docs = collection.aggregate([{"$match":{"email":useremail}},
			{"$group":{"_id":{"line":"$line"}}}])
		if docs:
			lineList = []
			for i in docs:
				lineList.append(i["_id"])
		return lineList
		

	def GetEsclation(self,useremail):
		rDict = {}
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
                        return
		
		collection = dbh.getCollection("andonMapMachines")
		if useremail == "admin@"+self.dbName+".com":
			docs = collection.aggregate([{"$match":{}},{"$group":{"_id":"$line",'finalDate':{"$last":"$date"},"assnList":{"$addToSet":"$assetNumber"}}}])
			rDict["designation"] = "admin"
		else: 
			lines = self.GetMapLines(useremail)
			docs = collection.aggregate([{"$match":{"$or":lines}},
			{"$group":{"_id":"$line",'finalDate':{"$last":"$date"},"assnList":{"$addToSet":"$assetNumber"}}}])
 
			rDict["rule"] = self.GetUserInfo(useremail)[0]["rule"]
			rDict["designation"] = self.GetUserInfo(useremail)[0]["designation"]
		docList = [i for i in docs]
                rDict["esclatedList"] = []
		for i in docList:
                        lineDict = {}
                        lineDict["line"] = i["_id"]
                        lineDict["escId"] = self.GetEsclationList(dbh,i["assnList"])
                        lineDict["escId"].sort(key=lambda x:x["mType"], reverse=True)
                        lineDict["escId"].sort(key=lambda x:x["dHours"], reverse=True)
                        rDict["esclatedList"].append(lineDict)
                rDict["esclatedList"].sort(key=lambda x:x["escId"][0]["mType"], reverse=False)

		return rDict
	def GetEsclationList(self,dbh,mList):
		queryDict = {}
		esclatedList = []
		queryDict["status"] = "esclated"
		collection = dbh.getCollection("andonEventsCollectionOfOperator")
		if collection is None:
			print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andonEventsCollectionOfOperator'
			return
		for i in mList:
			queryDict["machine_id"] = i
			docs = collection.find(queryDict,{"_id":False})
			docList = [j for j in docs]
			mCollection = dbh.getCollection("andonEventsCollectionOfMaintenance")
			if collection is None:
				print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andonEventsCollectionOfMaintenance'
				return
			if docList:
				for j in docList:
					tmpQueryDict = {}
					tmpQueryDict["machine_id"] = i
					mDocs = mCollection.aggregate([{"$match":tmpQueryDict},{"$group":{"_id":"$machine_id",'lsDate':{"$last":"$date"}}}])
					mDocList = [k for k in mDocs]
					dDiff = ( datetime.now() - j["date"]).seconds
					hrs,rem = divmod(dDiff,3600)
					minutes,rem = divmod(rem,60)
					tmpQueryDict["dHours"] = dDiff	
					tmpQueryDict["date"] = str(hrs) + ":"+str(minutes) +":" +str(rem)
					
					tmpQueryDict["mType"] = "danger"
					if mDocList:
						lastSolvedDate = mDocList[0]["lsDate"]
						if lastSolvedDate > j["date"]:
							tmpQueryDict["mType"] = "notice"
							esclatedList.append(tmpQueryDict)
							continue
					esclatedList.append(tmpQueryDict)
			else:
				tmpDict = {}
				tmpDict["dHours"] = 0
				tmpDict["machine_id"] = i
				tmpDict["mType"] = "success"
				esclatedList.append(tmpDict)

		return esclatedList
				
	def SetEventAction(self,actionDict):
		#dbh = mongoConnector.MongoConnector.getInstance()
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
		queryDict ={}
		queryDict["machine_id"] = actionDict["machine_id"]
		queryDict["status"] = "esclated"
		collection = dbh.getCollection("andonEventsCollectionOfOperator")
                if collection is None:
                        print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andonEventsCollectionOfOperator'
                        return
                docs = collection.find(queryDict,{"_id":False})
		docList = [i for i in docs]
		if actionDict["status"] == "solved":
			if docList:
				for i in docList:
					queryDict["date"] = i["date"]
					i["solved_by"] = actionDict["event_created_by"]
					i["solved_date"] = actionDict["date"]
					dbh.updateCollection_dash(queryDict,"andonEventsCollectionOfOperator" ,i)

			dbh.updateCollection_dash(actionDict,"andonEventsCollectionOfMaintenance" ,actionDict)	
		else:
			if docList:
				for i in docList:
					queryDict["date"] = i["date"]
					i["status"] = "verified"
					i["verified_by"] = actionDict["event_created_by"]
					i["verified_date"] = actionDict["date"]
					dbh.updateCollection_dash(queryDict,"andonEventsCollectionOfOperator" ,i)
		return	

					
	
	def GetCheckList(self,assetNumber):	
		checkList = {}
		checkList["demoFlag"] = self.demoFlag
		checkList["machineInfo"] = []
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
		if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
			return 
                collection = dbh.getCollection("andonMachinesInfoCollection")
		if collection is None:
			return
		docs = collection.find({'assetNumber':assetNumber},{"_id":False}).sort([("date",-1)]).limit(1)
		docList = [i for i in docs]
		if docList:
			checkListSerialNumber = docList[0]["checkListSerialNumber"]
                clCollection = dbh.getCollection("checklistsCollection")
		if clCollection is None:
			return
		docs = clCollection.find({'machineInfo.checklist_serial_number':checkListSerialNumber},{"_id":False}).sort([("date",-1)]).limit(1)
		docList =[i for i in docs]
		checkList["checkListSerialNumber"] = checkListSerialNumber
		if docList:
			qUserInfo	 = {}
			checkList["questionInfo"] = self.QuestionList(docList[0]["questions"],checkListSerialNumber,dbh)
			if checkList["questionInfo"] is None:
				return checkList
			for i in docList:
				checkList["heading"] = docList[0]["machineInfo"]["checklist_heading"]
				tmpDict = {}
				tmpDict[i["translation"]["plant"]] = i["machineInfo"]["plant"]
				tmpDict[i["translation"]["machine_equipment_workstation_name"]] = i["machineInfo"]["machine_equipment_workstation_name"]
				tmpDict[i["translation"]["pu_shop_dept"]] = i["machineInfo"]["pu_shop_dept"]
				#tmpDict[i["translation"]["checklist_heading"]] = i["machineInfo"]["checklist_heading"]
				tmpDict[i["translation"]["cell_circle"]] = i["machineInfo"]["cell_circle"]
				tmpDict[i["translation"]["machine_equipment_workstation_number"]] = i["machineInfo"]["machine_equipment_workstation_number"]
				checkList["machineInfo"].append(tmpDict)
		checkList["line"] = self.GetAssetNumberMappedInfo(assetNumber)
		return checkList
	def GetAssetNumberMappedInfo(self,assetNumber):
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
		if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
			return 
                collection = dbh.getCollection("andonMapMachines")
		if collection is None:
			return
		docs = collection.find({'assetNumber':assetNumber},{"_id":False}).sort([("date",-1)]).limit(1)
		docList = [i for i in docs]
		if docList:
			line = docList[0]["line"]
		return line

	def QuestionList(self,qList,checkListSerialNumber,dbh):
		queryDict = {}
		questionInfo = [] 
		queryDict["checklist_serial_number"] = checkListSerialNumber
		for i in qList:
			qOwners = []
			qStakeHolders = []
			tmpDict = {}
			if i["photo_location"] is not None:
				tmpDict["photo_location"] = self.dbName + '/checklists/' +checkListSerialNumber +'/'+i["photo_location"] 
			if i["photo_activity"] is not None:
				tmpDict["photo_activity"] =  self.dbName +'/checklists/' +checkListSerialNumber+'/'+ i["photo_activity"] 
			tmpDict["question_number"] = i["question_number"] 
			tmpDict["frequency"] = i["frequency"]
			tmpDict["displayOnly"] =  i["displayOnly"]
			questionInfo.append(tmpDict)
		return questionInfo

		
	def GetEsclationBreakeUp(self,useremail,machineId):
		cls = checkListStatus.CheckListStatus(self.dbName)
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
		queryDict = {}
		queryDict["machine_id"] = machineId
		queryDict["status"] = "esclated"
		#username = "shaheem@legacy.com"
		#username = "arshad@legacy.com"
                if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
		collection = dbh.getCollection("andonEventsCollectionOfOperator")
                if collection is None:
                        print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andonEventsCollectionOfOperator'
                        return
                docs = collection.find(queryDict,{"_id":False})
		docList = [i for i in docs]
		return docList	
	
	def GetPlantInfo(self):
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
		if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
		collection = dbh.getCollection("andonFactoryHierarchyCollection")
                if collection is None:
                        print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andonLineInfoCollection'
                        return
		docs = collection.find({"uploadInfo":"factoryHierarchy"},{"_id":False}).sort([("date",-1)]).limit(1)
		docList = [i for i in docs]
		if docList:
			date = docList[0]["date"]
			hList = docList[0]["headers"]
			hList.pop(0)
			hList.pop(0)
			hList.reverse()
		tmpDict = {}
		for i in range(0,len(hList)):
			tmpDict[hList[i]] = "$"+hList[i]
		collection = dbh.getCollection("andonLineInfoCollection")
		docs = collection.aggregate([
			{
			"$match":{"date":{"$gte":date}}
			},
			#{"$group":{"_id":tmpDict}}
                        #{"$group":{"_id":tmpDict,"mList":{"$addToSet":tmpDict}}},
			])
		docList = [i for i in docs]
				
	def GetScheduledInfo(self,useremail):
		skipHeader = {}
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
		if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
		collection = dbh.getCollection("andonFactoryHierarchyCollection")
                if collection is None:
                        print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andonLineInfoCollection'
                        return
		docs = collection.find({"uploadInfo":"factoryHierarchy"},{"_id":False}).sort([("date",-1)]).limit(1)
		docList = [i for i in docs]
		if docList:
			date = docList[0]["date"]
			hList = docList[0]["headers"]
			desigList = docList[0]["designationInfo"]
			hList.pop(0)
			hList.pop(0)
			hList.pop(0)
			hList.reverse()
			for i in hList:
				skipHeader[i] = False
			hList.append("line")#discard
		collection = dbh.getCollection("andonLineScheduleCollection")
		if useremail == "admin@"+self.dbName+".com":
                	docs = collection.aggregate([
                        	{
                        	"$match":{"status":"scheduled"}
                        	},
                        	{"$group":{"_id":"$status","sEmails":{"$addToSet":{"email":"$email","line":"$line"}}}}
                        	])
		else:
			lines = self.GetMapLines(useremail)
			docs = collection.aggregate([
                        	{
                        	"$match":{"status":"scheduled","$or":lines}
                        	},
                        	{"$group":{"_id":"$status","sEmails":{"$addToSet":{"email":"$email","line":"$line"}}}}
                        	])


	
		docList = [i for i in docs]
		scheduledEmail = []
		if docList:
			scheduledEmail = docList[0]["sEmails"]
		collection = dbh.getCollection("andonLineInfoCollection")
		skipHeader["_id"] = False
		skipHeader["date"] = False
		docs = collection.find(
			{
			"date":{"$gte":date}
			},skipHeader
			#{"$group":{"_id":tmpDict}}
                        #{"$group":{"_id":tmpDict,"mList":{"$addToSet":tmpDict}}},
			)
		docList = [i for i in docs]
		tmpList = []
		for i in docList:
			tmpDict = {}
			tmpDict["line"] = i["line"]
			for k,v in i.iteritems():
				if k not in hList:
					for j in i[k]:
						tmpDict["email"] = j["email"]
						if tmpDict in scheduledEmail:
							j["isScheduled"] = True
						else:
							j["isScheduled"] = False 
					#sList=sorted(i[k], key=lambda m:i[k][m][0]["hierarchyOrder"], reverse=True)
					#print sList
				desigList.append(k)
			i=collections.OrderedDict(sorted(i.items(), key=lambda n:desigList.index(n[0])))
			tmpList.append(i)
		return tmpList
	def GetAssetNumbers(self):
		rDict = {}
		lineList = []
		#rDict["assetNumbers"] = []
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
		if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
		collection = dbh.getCollection("andonFactoryHierarchyCollection")
                if collection is None:
                        print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andonLineInfoCollection'
                        return
		docs = collection.find({"uploadInfo":"machineInfo"},{"_id":False}).sort([("date",-1)]).limit(1)
		docList = [i for i in docs]
		if docList:
			lDate = docList[0]["date"]
		collection = dbh.getCollection("andonReserveMachines")
		docs = collection.aggregate([
			{"$group":{"_id":0,"assetNumbers":{"$addToSet":"$assetNumber"}}},
			])
		docList = [i for i in docs]
		if docList:
			rDict["reserveAssetNumbers"] = docList[0]["assetNumbers"]
		else:
			rDict["reserveAssetNumbers"] = [""]
		collection = dbh.getCollection("andonLineInfoCollection")
		docs = collection.aggregate([
			{"$group":{"_id":0,"lines":{"$addToSet":"$line"}}},
			])
		docList = [i for i in docs]
		if docList:
			lineList = docList[0]["lines"]
		collection = dbh.getCollection("andonMapMachines")
		docs = collection.aggregate([
			#{"$group":{"_id":{"line":"$line","assetNumber":"$assetNumber","lDate":{"$last":"$date"}}}},
			{"$group":{"_id":"$line",
				"assetNumbers":{"$addToSet":
							{"assetNumber":"$assetNumber","order":"$order"}
						}
				}
			},
			])
		docList = [i for i in docs]
		rDict["assetNumbersList"] = []
		for i in docList:
			for j in i["assetNumbers"] :
				if lineList:
					if i["_id"] in lineList:
						lineList.remove(i["_id"])

				if j["assetNumber"] in rDict["reserveAssetNumbers"]:
					removeDict = {}
					removeDict["assetNumber"] = j["assetNumber"]	
					removeDict["order"]	= j["order"]
					i["assetNumbers"].remove(removeDict)
					continue
			tDict = {}
			tDict["line"] = i["_id"]
			if i["assetNumbers"] :
				tDict["assetNumbers"] = i["assetNumbers"]
			else:
				tDict["assetNumbers"] = [""]
			rDict["assetNumbersList"].append(tDict)
		if lineList:
			for _k in lineList:
				tDict = {}
				tDict["assetNumbers"] = [""]	
				tDict["line"] = _k
				rDict["assetNumbersList"].append(tDict)
		rDict["assetNumbersList"].sort(key=lambda x:x["line"], reverse=False)#it is not generic we need to define order
		return rDict
	def MakeCall(self,assetNumber,line):
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
		if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
		collection = dbh.getCollection("andonLineScheduleCollection")
                if collection is None:
                        print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andonLineScheduleCollection'
                        return
		docs = collection.aggregate([
                                {
                                "$match":{"line":line}
                                },
                                {"$group":{"_id":"$line","mUInfo":{"$addToSet":{"phone":"$phone","designation":"$designation","time":"$Time","rule":"$rule"}}}}
                                ])

		docList = [i for i in docs]
		timeList = []
		if docList:
			print docList
			count = 0
			for i in docList[0]["mUInfo"]:
				timeList.append(Timer(20*i["time"],self.SetUpCall,[assetNumber,i["phone"],dbh]))
				timeList[count].start()
				count +=1
		return				
	def SetUpCall(self,assetNumber,phone,dbh):	
		print "esclation"
		collection = dbh.getCollection("andonEventsCollectionOfOperator")
                if collection is None:
                        print err.errDict["ERR_DB_COLLECTION_NONE"] % 'andonEventsCollectionOfOperator'
		docs = collection.find({"status":"esclated","machine_id":assetNumber},{"_id":False})
		docList = [i for i in docs]
		if docList:
			mg = MessageConnector()
			mg.MakeCall(phone)
			print "call to ",phone
		else:
			print "no call"
		return
	def GetPlantParams(self):
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
			print err.errDict["ERR_DB_NONE"] % self.dbName 
                        return
		
		collection = dbh.getCollection("andonMapMachines")
		#if useremail == "admin@"+self.dbName+".com":
		docs = collection.aggregate([{"$match":{}},{"$group":{"_id":"$line",'finalDate':{"$last":"$date"},"assnList":{"$addToSet":"$assetNumber"}}}])
		docList = [i for i in docs]
		print docList
		plantParamsList = []
		tmpDict = {}
		tmpDict["line"] = "All"
		tmpDict["machine"] = ["All"]
		plantParamsList.append(tmpDict)
		if docList:
			for i in docList:
				tmpDict = {}
				tmpDict["line"] = i["_id"]
				tmpDict["machine"] = []
				tmpDict["machine"] += i["assnList"] 
				tmpDict["machine"].insert(0,"All") 
				plantParamsList.append(tmpDict)
		return plantParamsList
 


if __name__ == '__main__':
	lg = Logic('legacy')
	lg.GetPlantParams()
	#rList = lg.getCheckListForFilling()
	#lg.PlantHealth("legacy",True)
	#lg.GetNumberOfUnFilledShift("bike")
	#lg.GetMapLines("shaheem@legacy.com")
	#lg.MakeCall("M/10","Line 2")
	#lg.GetAssetNumber()
	#date = datetime.today()
	#lis=lg.GetEsclationOptimised(date,"admin@legacy.com")
	#lis=lg.GetCheckListStatus(date,"admin@legacy.com")
	#lg.GetBreakup("admin@legacy.com",lis[1])
	#lg.GetCheckList(rList[0])
	#endDate= datetime.today() - timedelta(30)
        #dateList = []
        #for i in range(1,31):
        #	dateList.append(endDate + timedelta(days=i))
	#for j in dateList:
	#	lg.PercentageUnFilledCheckList("admin",True,j)

