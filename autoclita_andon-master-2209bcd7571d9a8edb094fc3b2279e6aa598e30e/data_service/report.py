from datetime import datetime,timedelta
import pandas as pd
from clita_legacy.errcode import errcode as err
from clita_legacy.dataconnector import mongoConnector
class Report:
	def __init__(self,dbName):
		self.dbName = dbName
	def GetPlantHealth(self,params):
		print "params==\n\n\n\n\n\n",type(params["line"])
		startTime = datetime.today() - timedelta(days=30) #upload date
		endTime = datetime.today()
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
                        print err.errDict["ERR_DB_NONE"] % self.dbName 
                        return
                collection = dbh.getCollection("andonEventsCollectionOfOperator")
                if collection is None:
                        print err.errDict["ERR_DB_COLLECTION_NONE"] % self.dbName
                        return
		diffDays = (endTime - startTime).days
		#if params["line"] == "All":
		#	tmpList = {}
		plantHealth = {}
		if params["line"] == "All":
			docs = collection.find({},{"_id":False})
			docList = [i for i in docs]
			plantHealth["MTTR"] = self.GetMTTR(docList)["MTTR"]
		elif params["machine"] == "All":
			docs = collection.find({"line":params["line"]},{"_id":False})
			docList = [i for i in docs]
			plantHealth["MTTR"] = self.GetMTTR(docList)["MTTR"]
		else:
			docs = collection.find(params,{"_id":False})
			docList = [i for i in docs]
			plantHealth["MTTR"] = self.GetMTTR(docList)["MTTR"]


		return plantHealth

		
	def GetMTTR(self,docList):
		mttrInfo = {}
		mttrInfo["MTTR"] = str(0)+"Hrs" + ":"+str(0) +"minutes:" +str(0) + " seconds"
		if docList:
			labels = ["machine_id","date","line","solved_date"]
			df = pd.DataFrame.from_records(docList,columns=labels)
			df["TR"] = df["solved_date"] - df["date"]
			length = len(df["TR"])
			df=df[df["TR"].notnull()]
			Sum = df["TR"].sum()
			try:
				MTTR = Sum/length
				MTTR = MTTR.seconds
				hrs,rem = divmod(MTTR,3600)
                                minutes,rem = divmod(rem,60)
			except:
				print "error"
                        mttrInfo["MTTR"] = str(hrs)+"Hrs" + ":"+str(minutes) +"minutes:" +str(rem) + " seconds"
		return mttrInfo
	def GetMTBF(self,docList):
		mtbfInfo = {}
		mtbfInfo["MTBF"] = str(0)+"Hrs" + ":"+str(0) +"minutes:" +str(0) + " seconds"
		if docList:
			docList.sort(key=lambda x:x["date"], reverse=True)
				
				
                        mtbfInfo["MTBF"] = str(hrs)+" Hrs" + ":"+str(minutes) +" minutes:" +str(rem) + " seconds"
		return mtbfInfo

