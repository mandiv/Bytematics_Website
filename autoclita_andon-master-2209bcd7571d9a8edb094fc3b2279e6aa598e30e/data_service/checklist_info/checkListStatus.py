from clita_legacy.dataconnector import mongoConnector
from clita_legacy.errcode import errcode as err
from datetime import date,time,datetime,timedelta
import collections
class CheckListStatus:
	def __init__(self,dbName):
		self.dbName = dbName	
	
	def GetNumberOfShifts(self,date,checkListSerialNumber,dbh):
                shiftCollection = dbh.getCollection("shiftCollection")
                if shiftCollection is None:
                        print err.errDict["ERR_COLLECTION_NONE"] % 'shiftCollection'
                        return
                unScheduleCollection = dbh.getCollection("unScheduleCollection")
                if unScheduleCollection is None:
                        print err.errDict["ERR_DB_COLLECTION_NONE"] % 'unScheduleCollection'
                unScheduleShifts = unScheduleCollection.find({"datetime":{"$gte":date},"checklist_serial_number":checkListSerialNumber}).count()
                shiftDocs = shiftCollection.find({},{"_id":False}).sort([("date",-1)]).limit(1)
                shiftList = [i for i in shiftDocs]
                numberOfShift = 0
                if shiftList:
                        numberOfDays = datetime.today() - date
                        lastShiftChange = shiftList[0]["date"]
                if lastShiftChange >= date:
                        shiftDocs = shiftCollection.find({"date":{"$gte":date,"$lte":datetime.today()}},{"_id":False})
                        if shiftDocs:
                                for i in shiftDocs:
                                        numberOfDays = i["date"] - date
                                        numberOfShift += (numberOfDays*len(i["sList"])).days
                                        date = i["date"]
                else:
                        numberOfShift = ((numberOfDays)*(len(shiftList[0]["sList"]))).days

                numberOfWorkingShift = numberOfShift - unScheduleShifts
                return numberOfWorkingShift,unScheduleShifts
	
	def GetEsclationOptimised(self,date,useremail):
                dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                oCollection = dbh.getCollection("eventsCollectionOfOperator")
                if oCollection is None:
                        print err.errDict["ERR_DB_COLLECTION_NONE"] % 'eventsCollectionOfOperator'
                        return
                mCollection = dbh.getCollection("eventsCollectionOfMaintenance")
                mDocs = mCollection.aggregate(
                        [
                                {"$match":{"date":{"$lte":date}}} ,
                                {"$project":{"question_number":1,"checklist_serial_number":1,"shift":1,"date":1}},
                                {"$group":{"_id":{"question_number":"$question_number","checklist_serial_number":"$checklist_serial_number"},'finalDate':{"$last":"$date"}}}
                        ])
                mDocList = [j for j in mDocs]
                checkListFrequencyInfo = dict()
                skippingQuestions = []
                for _k in mDocList:
                        tmpDict = {}
                        esclationCount = oCollection.find({"question_number":_k["_id"]["question_number"],"checklist_serial_number":_k["_id"]["checklist_serial_number"],"status":"notok","date":{"$gte":_k["finalDate"]},"$or":[{"owners_email.owner_email":useremail},{"stakeholders_email.stakeholder_email":useremail}]},{"_id":False}).count()
                        if esclationCount > 0:
                                tmpDict = {}
                                tmpDict["question_number"] = _k["_id"]["question_number"]
                                tmpDict["checklist_serial_number"] = _k["_id"]["checklist_serial_number"]
                                skippingQuestions.append(tmpDict.copy())
                                del tmpDict["checklist_serial_number"]
                                tmpDict["esclationCount"] = esclationCount
                                if _k["_id"]["checklist_serial_number"] in checkListFrequencyInfo.keys():
                                        checkListFrequencyInfo[_k["_id"]["checklist_serial_number"]].append(tmpDict)
                                else:
                                        checkListFrequencyInfo[_k["_id"]["checklist_serial_number"]] = []
                                        checkListFrequencyInfo[_k["_id"]["checklist_serial_number"]].append(tmpDict)
			else:
				tmpDict = {}
                                tmpDict["question_number"] = _k["_id"]["question_number"]
                                tmpDict["checklist_serial_number"] = _k["_id"]["checklist_serial_number"]
                                skippingQuestions.append(tmpDict.copy())
	
		oDocs = oCollection.aggregate(
                        [
                                {"$match":{"date":{"$lte":date},"status":"notok"}} ,
                                {"$project":{"question_number":1,"checklist_serial_number":1,"shift":1,"date":1}},
                                {"$group":{"_id":{"question_number":"$question_number","checklist_serial_number":"$checklist_serial_number"},'count':{"$sum":1}}},

                                {"$project":{"tmp":{"data":"$_id","esclationCount":"$count"}}},
                                {"$group":{"_id":"$tmp"}},
                        ])

                docList = [i for i in oDocs]
                if docList:
                        for i in docList:
                                if i["_id"]["data"] not in skippingQuestions:
                                        tmpDict = {}
                                        tmpDict["esclationCount"] = i["_id"]["esclationCount"]
                                        if i["_id"]["data"].get("question_number") is None:
                                                continue
                                        tmpDict["question_number"] = i["_id"]["data"]["question_number"]
                                        if i["_id"]["data"]["checklist_serial_number"] in checkListFrequencyInfo.keys():
                                                checkListFrequencyInfo[i["_id"]["data"]["checklist_serial_number"]].append(tmpDict)
                                        else:
                                                checkListFrequencyInfo[i["_id"]["data"]["checklist_serial_number"]] = []
                                                checkListFrequencyInfo[i["_id"]["data"]["checklist_serial_number"]].append(tmpDict)

                esclationList = []
                esclatedCheckList = []
                for k,v in checkListFrequencyInfo.iteritems():
                        tmpDict ={}
                        tmpDict["checklist_serial_number"] = k
                        esclatedCheckList.append(k)
                        v.sort(key=lambda x:x["esclationCount"], reverse=True)
                        tmpDict["questions"] = v
                        esclationList.append(tmpDict)
		esclationList.sort(key=lambda x:x["questions"][0]["esclationCount"], reverse=True)
                return esclationList,esclatedCheckList
	
	def GetEsclationBreakUp(self,useremail,esclationDict):
                adminUserFormats = [".com",".co.in"]
                dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                collection = dbh.getCollection("checklistsCollection")

                esclationDetails = {}
                esclationDetails["checklist_serial_number"] = esclationDict["checklist_serial_number"]
                esclationDetails["questions"] = []
                #lg = Logic('legacy')
                breakUpInfoList = []
                #esclatList = lg.GetEsclationOptimised(date,"admin@legacy.com")
                #esclationDict = esclatList[0]
                docs = collection.find({'machineInfo.checklist_serial_number':esclationDict["checklist_serial_number"]},{"_id":False}).sort([("date",-1)]).limit(1)
                docList =[i for i in docs]
                escalatedQuestionsList = []
		esclatedQInfo = {}
                for i in esclationDict["questions"]:
			esclatedQInfo[i["question_number"]] = i["esclationCount"]
                        escalatedQuestionsList.append(i["question_number"])
                if docList:
                        ownerEmail = []
                        ownerEmail = ownerEmail + docList[0]["machineInfo"]["owner_email"]
                        if "admin@" in useremail:
                                ownerEmail.append(useremail)
                        stakeHolderEmail = []
                        stakeHolderEmail = stakeHolderEmail + docList[0]["machineInfo"]["stakeholder_email"]
                        for i in docList[0]["questions"]:
                                tmpDict = {}
                                tmpDict["role"] = None
                                if i["question_number"] not in escalatedQuestionsList:
                                        continue
                                else:
                                        tmpDict["question_number"] = i["question_number"]
                                        #tmpDict["frequency"] = i["escalationCount"]
					if tmpDict["question_number"] in esclatedQInfo.keys():
                                        	tmpDict["frequency"] = esclatedQInfo[tmpDict["question_number"]]
					tmpDict["displayOnly"] = i["displayOnly"]
					if useremail in ownerEmail:
                                        #for j in i["owner_email"]
                                                tmpDict["role"] ="owner"
                                        else:
                                                if  useremail in stakeHolderEmail:
                                                        tmpDict["role"] ="stakeholder"
                                                else:
                                                        for j in i["owner_email"]:
                                                                if useremail in j:
                                                                        tmpDict["role"] ="owner"
                                                                        break
                                                        if tmpDict["role"] is None:
                                                                for j in i["stakeholder_email"]:
                                                                        if useremail in j:
                                                                                tmpDict["role"] ="stakeholder"
                                                                                break

                                esclationDetails["questions"].append(tmpDict)
				esclationDetails["questions"].sort(key=lambda x:x["frequency"], reverse=True)
                return esclationDetails


	def GetCheckListHealthIndicator(self,checkListSerialNumber,rule):
                dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
                        print err.errDict["ERR_DB_NONE"] % self.dbName
                        return
                collection = dbh.getCollection("eventsCollectionOfOperator")
                if collection is None:
                        return
                date = datetime.today()
                if rule =="unFilledCheckList":
                        docs = collection.find({'checklist_serial_number':checkListSerialNumber},{"_id":False}).sort([("date",-1)]).limit(1)
                        docList = [i for i in docs]
                        if docList:
                                date = docList[0]["date"]
                        else:
                                cLCollection = dbh.getCollection("checklistsCollection")
                                if cLCollection is None:
                                        return
                                cLDocs = cLCollection.find({'machineInfo.checklist_serial_number':checkListSerialNumber},{"_id":False}).sort([("date",-1)]).limit(1)
                                cLDocList = [i for i in cLDocs]
                                date = cLDocList[0]["date"]
                elif rule=="filledCheckList":
                        docs = collection.find({'checklist_serial_number':checkListSerialNumber,"status":"notok"},{"_id":False}).sort([("date",-1)]).limit(1)
                        docList = [i for i in docs]
                        if docList:
                                date = docList[0]["date"]
                        else:
                                clCollection = dbh.getCollection("checklistsCollection")
                                if clCollection is None:
                                        return
                                clDocs = clCollection.find({'machineInfo.checklist_serial_number':checkListSerialNumber},{"_id":False}).sort([("date",-1)]).limit(1)
                                clDocList = [i for i in clDocs]
                                date = clDocList[0]["date"]

                numberOfWorkingDays,unScheduleShifts = self.GetNumberOfShifts(date,checkListSerialNumber,dbh)
                return numberOfWorkingDays


if __name__ == '__main__':
        cls = CheckListStatus('legacy')
        #rList = lg.getCheckListForFilling()
        #lg.PlantHealth("legacy",True)
        #lg.GetNumberOfUnFilledShift("bike")
        date = datetime.today()
        lis=cls.GetEsclationOptimised(date,"admin@legacy.com")
        #lis=lg.GetCheckListStatus(date,"admin@legacy.com")
        #lg.GetBreakup("admin@legacy.com",lis[1])
        #lg.GetCheckList(rList[0])
                                         
                                                                         
