
1. On server:

 1. mkdir my_project.git
 2. cd my_project.git
 3. git --bare init
2. On client:

 1. mkdir my_project
 2. cd my_project
 3. touch .gitignore


 4. git init
 5. git add .
 6. git commit -m "Initial commit"
 7. git remote add origin youruser@yourserver.com:/path/to/my_project.git
 8. git push origin master


----
 Note that when you add the origin, there are several formats and schemas you could use. I recommend you see what your hosting service provides.


